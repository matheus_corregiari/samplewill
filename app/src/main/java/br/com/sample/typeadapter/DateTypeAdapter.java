package br.com.sample.typeadapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.IOException;

import br.com.sample.model.DateType;

public final class DateTypeAdapter extends TypeAdapter<DateType> {

    public static final DateTimeFormatter API_FORMAT =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX");

    @Override
    public DateType read(JsonReader in) throws IOException {
        if (!in.hasNext()) return null;

        final String date = in.nextString();
        final LocalDateTime dateTime = LocalDateTime.parse(date, API_FORMAT);
        return new DateType(dateTime);
    }

    @Override
    public void write(JsonWriter out, DateType value) throws IOException {
        if (value != null) out.value(value.getApiFormat());
    }
}
