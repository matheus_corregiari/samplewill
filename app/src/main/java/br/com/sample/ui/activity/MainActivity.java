package br.com.sample.ui.activity;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.sample.databinding.ActivityMainBinding;
import br.com.sample.model.Contact;
import br.com.sample.model.DateType;
import br.com.sample.typeadapter.DateTypeAdapter;
import br.com.sample.ui.adapter.ContactRecyclerAdapter;
import timber.log.Timber;

import static br.com.sample.filter.ArrayFilter.filterDuplicates;

public final class MainActivity extends AppCompatActivity {

    // Constants
    public static final String STATE_ADAPTER = "STATE_ADAPTER";

    // Final Variables
    private final ContactRecyclerAdapter adapter = new ContactRecyclerAdapter();

    // Variables
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setupRecyclerView();
        if (savedInstanceState == null) loadContacts();
        else adapter.restoreInstanceState(savedInstanceState.getBundle(STATE_ADAPTER));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(STATE_ADAPTER, adapter.saveInstanceState());
    }

    // Setup Methods
    private void setupRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        binding.recyclerView.setAdapter(adapter);
    }

    // Load Data Methods
    private void loadContacts() {
        try {
            final List<Contact> contacts = parseContactList(loadJsonAsset("contacts.json"));
            filterDuplicates(contacts);
            adapter.setList(contacts);
        } catch (IOException e) {
            Timber.e(e, "Error parsing Json File");
        }
    }

    // Auxiliar Methods
    @NonNull
    private String loadJsonAsset(@NonNull final String asset) throws IOException {
        final AssetManager assets = getAssets();
        final InputStream is = assets.open(asset);
        final int size = is.available();
        final byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return new String(buffer, "UTF-8");
    }

    @NonNull
    private List<Contact> parseContactList(@NonNull final String contactListAsJsonString) throws IOException {
        final Gson gson = new GsonBuilder().registerTypeAdapter(DateType.class, new DateTypeAdapter()).create();
        final List<Contact> contacts = new ArrayList<>();
        contacts.addAll(Arrays.asList(gson.fromJson(contactListAsJsonString, Contact[].class)));
        return contacts;
    }

}