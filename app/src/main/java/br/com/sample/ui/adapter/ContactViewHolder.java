package br.com.sample.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import br.com.sample.databinding.ItemContactBinding;
import br.com.sample.model.Contact;

final class ContactViewHolder extends RecyclerView.ViewHolder {

    private final ItemContactBinding binding;

    ContactViewHolder(@NonNull final ItemContactBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    void bind(@NonNull final Contact contact) {
        binding.setContact(contact);
        binding.executePendingBindings();
    }
}