package br.com.sample.ui.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.sample.databinding.ItemContactBinding;
import br.com.sample.model.Contact;

public final class ContactRecyclerAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    // Constants
    private static final String STATE_LIST = "STATE_LIST";

    // Variables
    private List<Contact> list = new ArrayList<>();
    private LayoutInflater inflater;

    public void setList(@NonNull final List<Contact> list) {
        this.list = list;
        notifyItemChanged(0, this.list.size());
    }

    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return new ContactViewHolder(ItemContactBinding.inflate(getLayoutInflater(parent), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactViewHolder holder, final int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @NonNull
    private LayoutInflater getLayoutInflater(@NonNull final ViewGroup parent) {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.getContext());
        return inflater;
    }

    @NonNull
    public Bundle saveInstanceState() {
        final Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(STATE_LIST, (ArrayList<Contact>) list);
        return bundle;
    }

    public void restoreInstanceState(@Nullable final Bundle bundle) {
        if (bundle == null) return;
        list = bundle.getParcelableArrayList(STATE_LIST);
    }
}