package br.com.sample.filter;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.ListIterator;

public final class ArrayFilter {

    public ArrayFilter() { }

    /**
     * This method removes all duplicate elements from a list, passed by reference
     *
     * @param list List do filter duplicates
     */
    public static void filterDuplicates(@NonNull final List list) {
        for (final ListIterator iterator = list.listIterator(); iterator.hasNext(); ) {
            final Object contact = iterator.next();
            final int indexOf = list.indexOf(contact);
            final int lastIndexOf = list.lastIndexOf(contact);
            if (indexOf != lastIndexOf) iterator.remove();
        }
    }
}
