package br.com.sample.binding;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public final class PicassoBindingAdapter {

    private PicassoBindingAdapter() { }

    @BindingAdapter(value = {"android:src", "placeholder"})
    public static void setImageUrl(@NonNull final ImageView view, @NonNull final String url, final int placeHolder) {
        Picasso.with(view.getContext()).load(url).error(placeHolder).placeholder(placeHolder).into(view);
    }

}