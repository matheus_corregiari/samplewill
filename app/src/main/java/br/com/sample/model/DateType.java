package br.com.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.Locale;

import br.com.sample.typeadapter.DateTypeAdapter;

public final class DateType implements Parcelable {

    private final LocalDateTime localDateTime;

    public DateType(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getApiFormat() {
        return ZonedDateTime.of(localDateTime, ZoneId.systemDefault()).format(DateTypeAdapter.API_FORMAT);
    }

    public String getFormattedDate() {
        return localDateTime.format(
                DateTimeFormatter.ofPattern("dd/MMMM/yyyy", new Locale("pt", "BR")));
    }

    // Parcelable Impl
    public static final Creator<DateType> CREATOR = new Creator<DateType>() {
        @Override
        public DateType createFromParcel(Parcel in) {
            return new DateType(in);
        }

        @Override
        public DateType[] newArray(int size) {
            return new DateType[size];
        }
    };

    private DateType(Parcel in) {
        localDateTime = (LocalDateTime) in.readSerializable();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(localDateTime);
    }
}
