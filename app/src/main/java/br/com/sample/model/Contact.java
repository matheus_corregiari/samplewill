package br.com.sample.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

public final class Contact implements Parcelable {

    @Expose private String id;
    @Expose private String name;
    @Expose private String image;
    @Expose private DateType birthday;
    @Expose private String bio;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public DateType getBirthday() {
        return birthday;
    }

    public String getBio() {
        return bio;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Contact && id != null && id.equals(((Contact) obj).id);
    }

    // Parcelable Impl
    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(@NonNull final Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(final int size) {
            return new Contact[size];
        }
    };

    private Contact(@NonNull final Parcel in) {
        id = in.readString();
        name = in.readString();
        image = in.readString();
        birthday = in.readParcelable(DateType.class.getClassLoader());
        bio = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull final Parcel dest, final int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeParcelable(birthday, flags);
        dest.writeString(bio);
    }
}
