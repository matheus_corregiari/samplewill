#### Gson ####
-keepattributes Signature
-keepattributes *Annotation*
-keepattributes EnclosingMethod
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

#### Retrolambda ####
-dontwarn java.lang.invoke.*

#### Picasso ####
-dontwarn com.squareup.okhttp.**